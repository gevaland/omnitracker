package main
import (
	"ru.omnicomm.ns/server"
	"ru.omnicomm.ns/shared"
)

func main() {
	println("Communication Server is happen to listen you!")

	shared.GetAppConfig()
	server.Run()
	<-make(chan bool)
}
