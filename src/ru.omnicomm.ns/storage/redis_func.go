package storage

import (
	"log"
	"github.com/gosexy/redis"
	"ru.omnicomm.ns/shared"
	"strconv"
)

func WriteLastMessageIndex(lastDataIndex uint, terminalId uint) error {
	log.Printf("Writting last data index to the Redis.")

	client := newClient()
	defer client.Quit()

	terminalIdStr := strconv.Itoa(int(terminalId))

	client.Del(terminalIdStr)
	_, err := client.Set(terminalIdStr, lastDataIndex)

	return err
}

func ReadLastMessageIndex(terminalId uint) (uint, error) {
	log.Printf("Reading last data index from the Redis.")

	client := newClient()
	defer client.Quit()

	terminalIdStr := strconv.Itoa(int(terminalId))

	// client.Incr(strconv.Itoa(int(terminalIdStr)))
	value, err := client.Get(terminalIdStr)
	if err != nil {
		log.Printf("Fail to GET: %s.", err.Error())
		return 0, err
	}

	valueInt, _ := strconv.Atoi(value)
	valueUint := uint(valueInt)
	return valueUint, err
}

func newClient() *redis.Client {
	client := redis.New()
    log.Printf("Redis: %s, %d.", shared.GetAppConfig().RedisAddr, shared.GetAppConfig().RedisPort)
	err := client.Connect(shared.GetAppConfig().RedisAddr, shared.GetAppConfig().RedisPort)
	if err != nil {
		log.Fatalf("Fail: connect to the Redis is failed: %s\n", err.Error())
	}

	log.Printf("Info: connected to the Redis.")

	return client
}
