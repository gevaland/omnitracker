package server

import (
	"log"
	"net"
	"encoding/hex"
	"ru.omnicomm.ns/dispatcher"
	"ru.omnicomm.ns/shared"
	"strconv"
	"bufio"
	"io"
)

const BUF_SIZE = 4096

// Starts a TCP server and waits infinitely for connections.
func Run() net.Listener {
	log.Printf("Starting to listen.")

	listener := connect()
	sendContract()
	go waitForClient(listener)

	return listener
}

func connect() net.Listener {
	addr := shared.GetAppConfig().ServerAddr
	port := strconv.Itoa(int(shared.GetAppConfig().ServerPort))

	tcpAddr, error := net.ResolveTCPAddr("tcp", addr + ":" + port)
	if error != nil {
		log.Fatalf("Fail: could not resolve address.")
	}

	listener, error := net.Listen(tcpAddr.Network(), tcpAddr.String())
	if error != nil {
		log.Fatalf("Fail: cannot listen on this port: %s.", error.Error())
	}

	return listener
}

func waitForClient(listener net.Listener) {
	for {
		log.Printf("Func: waiting for clients.")

		conn, error := listener.Accept()
		if error != nil {
			log.Fatalf("Fail: client error: %s.", error)
		}
		log.Printf("Info: accepted connection.")

		client := shared.NewClient()

		go redirectInput(client, conn)
		go redirectOutput(client, conn)
		go dispatcher.DispatchClientHandle(client)

		log.Printf("End: client handled.")
	}
}

// Reads incoming data from client.Incoming channel,
// sends it to the client.Outgoing channel handled by #handleClientWrite(...).
func redirectInput(client *shared.Client, conn net.Conn) {
	defer conn.Close()

	data := make([]byte, BUF_SIZE)
	reader := bufio.NewReader(conn)
	for {
		<-client.Handler.InReady
		length, err := reader.Read(data)
		log.Printf("Debug: reading data from the socket \n%s", hex.Dump(data))
		if err != nil && err != io.EOF {
			log.Printf("Error: cannot read from client ID %d: %s.", client.Id, err.Error())
			return
		}
		if length == 0 {
			continue
		}

		data = data[:length]
		// log.Printf("Debug: received data \n%s", hex.Dump(data))

		client.Handler.In <-data
	}
}

// Reads incoming data from client.Incoming channel,
// sends it to the client.Outgoing channel (to be picked up by IOHandler).
func redirectOutput(client *shared.Client, writer net.Conn) {
	defer writer.Close()

	for {
		out :=<-client.Handler.Out
		log.Printf("Info: sending data \n%s", hex.Dump(out))

		_, err := writer.Write(out)
		if err != nil {
			log.Printf("Error: cannot write to client ID: %d.", client.Id)
		}
	}
}
