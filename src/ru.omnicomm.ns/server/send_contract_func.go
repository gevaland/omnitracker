package server

import (
    "log"
    "ru.omnicomm.ns/amq_stomp"
    "ru.omnicomm.ns/descriptor"
)

func sendContract() {
    log.Printf("Func: sending the contract.")

	err := amq_stomp.SendContract(descriptor.NewContract())
    if err != nil {
        log.Fatalf("Fail: cannot send the contract: %s", err.Error())
    }
    log.Printf("End: sent the contract.")
}
