package optima2

import (
	"log"
	"bytes"
	"encoding/hex"
	"encoding/binary"
	"ru.omnicomm.ns/shared"
	"ru.omnicomm.ns/storage"
	"ru.omnicomm.ns/xxtea_patched"
	"ru.omnicomm.ns/amq_stomp"
)

const crc_len = 2
const pkg_prefix_len = 4

func Handle(client *shared.Client) [][]byte {
	part1 := make([]byte, 0)
	for {
		for len(part1) - bytes.LastIndexByte(part1, 0xc0) < 4 || bytes.LastIndexByte(part1, 0xc0) < 0 {
			client.Handler.InReady <-true
			part1 = append(part1, <-client.Handler.In...)
		}
		part1 = part1[bytes.LastIndexByte(part1, 0xc0):] // FIXME: sometimes appendix part is not started with c0

		log.Printf("Func: handling part1 %s...%s of length %d.",
			hex.EncodeToString(part1[0:2]), hex.EncodeToString(part1[len(part1)-2:len(part1)]), len(part1))
		if part1[0] != 0xc0 {
			log.Fatalf("Fail: cannot handle part1 because it first byte != 0xc0.")
		}

		dataLength := int(binary.LittleEndian.Uint16(part1[2:4]))
		log.Printf("Info: data length is %d.", dataLength)

		log.Printf("Info: pkg length is %d.", pkg_prefix_len + dataLength + crc_len)

		part2 := part1
		for len(part2) < pkg_prefix_len + dataLength + crc_len {
			client.Handler.InReady <-true
			part2 = append(part2, <-client.Handler.In...)
		}

		delta, part3 := cutEscBytes(part2)
		if delta > 0 {
			log.Printf("Info: delta is %d.", delta)
			for len(part3) < pkg_prefix_len + dataLength + crc_len {
				client.Handler.InReady <-true
				part3 = append(part3, <-client.Handler.In...)
			}
		}
		log.Printf("Info: part3 handled %s...%s of length %d.",
			hex.EncodeToString(part3[:2]), hex.EncodeToString(part3[len(part3)-2:len(part3)]), len(part3))

		pkg := part3[:pkg_prefix_len+dataLength+crc_len]
		log.Printf("Info: extracted pkg %s...%s of length %d.",
			hex.EncodeToString(pkg[:2]), hex.EncodeToString(pkg[len(pkg)-2:len(pkg)]), len(pkg))

		if len(part3) > pkg_prefix_len + dataLength + crc_len  {
			log.Printf("Info: detected another part1 in part3 \n%s", hex.Dump(part1))
			part1 = part3[pkg_prefix_len+dataLength+crc_len:]
		} else {
			part1 = make([]byte, 0)
		}

		data := pkg[pkg_prefix_len:pkg_prefix_len+dataLength]
		if len(data) != dataLength {
			log.Fatalf("Fail: expected data length of %d if different from actual %d.", dataLength, len(data))
		}
		if len(data) > 2 {
			log.Printf("Info: data handled %s...%s of length %d.",
				hex.EncodeToString(data[:2]), hex.EncodeToString(data[len(data)-2:len(data)]), len(data))
		}

		// FIXME
		//expectedCrc := pkg[pkg_prefix_len+dataLength:pkg_prefix_len+dataLength+crc_len]
		//resultCrc := calcCrcOfBytes(pkg[1:pkg_prefix_len+dataLength])
		//if !bytes.Equal(expectedCrc, resultCrc)  {
		//	log.Fatalf("Fail: expected CRC is %s, but result CRC is %s.", hex.Dump(expectedCrc), hex.Dump(resultCrc))
		//} else {
		//	log.Printf("Info: CRC is OK.")
		//}

		switch pkg[1] {
		case 0x80:
			client.Id = requestAuth(data, client).Id
		case 0x83:
			requestData(data, client)
		case 0x86:
			if dataLength > 0 {
				processData(data, client)
			} else {
				requestClearData(client)
			}
		case 0x88:
			log.Printf("Terminal cleared data.")
		case 0x9F:
			if dataLength > 0 {
				processData(data, client)
			} else {
				requestClearData(client)
			}
			requestAcceptWifiData(client)
		case 0x95:
			processData(data, client)
		default:
			// due requirements wrong request can't fail server
			log.Printf("Error: unexpected msg code %s.", hex.Dump([]byte { pkg[0], pkg[1] }))
		}

		log.Printf("End: handled pkg.")
	}
}

func cutEscBytes(data []byte) (int, []byte) {
	result := make([]byte, 0)
	isDB := false
	delta := 0
	for _, oneByte := range data {
		if isDB {
			switch oneByte {
			case 0xdc:
				result = append(result, 0xc0)
				delta++
			case 0xdd:
				result = append(result, 0xdb)
				delta++
			default:
				result = append(result, 0xdb)
			}
			isDB = false
		} else if oneByte == 0xdb {
			isDB = true
		} else {
			result = append(result, oneByte)
		}
	}

	return delta, result
}

func addEscBytes(data []byte) []byte {
	result := make([]byte, 0)

	for _, oneByte := range data {
		switch oneByte {
		case 0xc0:
			result = append(result, 0xdb, 0xdc)
			continue
		case 0xdb:
			result = append(result, 0xdb, 0xdd)
			continue
		default:
			result = append(result, oneByte)
		}
	}

	return result
}

func requestAuth(data []byte, client *shared.Client) *shared.Client {
	log.Printf("Requesting auth for client.")

	terminalId := shared.ExtractTerminalId(data)
	client.Id = terminalId

	log.Printf("Info: generate token for terminal ID: %d.", terminalId)

	zeros := []byte {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	}

	passw := xxtea_patched.Encrypt(zeros, shared.Make4x4bytes(terminalId))
	if length := len(passw); length < 32 {
		log.Fatalf("Fail: length of password is %d instead of >=32.", length)
	}

	left := passw[0:16]
	// right := passw[16:32]
	token := zeros[0:16]

	encryptedToken := xxtea_patched.Encrypt(token, left)
	if length := len(encryptedToken); length != 16 {
		log.Fatalf("Fail: length of encryped token is %d instead of 16.", length)
	}

	response := []byte { 0x82, 0x10, 0x00 }
	response = append(response, encryptedToken...)

	crc := calcCrcOfBytes(response)
	response = append(response, crc[1], crc[0])

	response = addEscBytes(response)
	response = append([]byte { 0xC0 }, response...)

	client.Handler.Out <-response

	log.Printf("Success: sent check for auth.")

	return client
}

func checkAuth(data []byte) error {
	return nil
}

func requestData(data []byte, client *shared.Client) {
	log.Printf("Func: checking auth from client.")

	if err := checkAuth(data); err != nil {
		log.Fatalf("Fail: unexpected token.")
	}

	dataId, _ := storage.ReadLastMessageIndex(client.Id)
	dataIdBytes := shared.Make4bytes(dataId)
	log.Printf("Info: prepared dataId \n%s for the request.", hex.Dump(dataIdBytes))

	response := []byte { 0x85, 0x04, 0x00 }
	response = append(response, dataIdBytes...)

	crc := calcCrcOfBytes(response)
	response = append(response, crc[1], crc[0])

	response = addEscBytes(response)
	response = append([]byte { 0xC0 }, response...)

	client.Handler.Out <-response

	log.Printf("Success: sent data request.")
}

func processData(data []byte, client *shared.Client) {
	log.Printf("Func: processing data starting with %s...%s of length %d.",
		hex.EncodeToString(data[0:2]), hex.EncodeToString(data[len(data) - 2:len(data)]), len(data))

	var msgs [][]byte
	client.LastMsgId, msgs = parseTransportData(data)
	log.Printf("Info: parsed data \n%s", hex.Dump(data))
	for i, msg := range msgs {
		terminalId := client.Id
		msgId := client.LastMsgId * 10 + uint(i)
		log.Printf("Debug: message ID is %d.", msgId)
		recReg := deserialiseToRecReg(msg)
		nsBinaryDataInProto := serialiseToNsBinaryDataMessage(recReg, &terminalId, &msgId)
		amq_stomp.SendMessage(nsBinaryDataInProto)
		storage.WriteLastMessageIndex(client.LastMsgId, client.Id)
	}

	log.Printf("End: processed data.")
}

// Sends response with last accepted msg ID. Optional as Anton said
func requestAcceptWifiData(client *shared.Client) {
	response := []byte { 0xA0, 0x04, 0x00, }
	msgIdBytes:= shared.Make4bytes(client.LastMsgId)
	response = append(response, msgIdBytes...)

	crc := calcCrcOfBytes(response)
	response = append(response, crc[1], crc[0])

	response = addEscBytes(response)
	response = append([]byte { 0xC0 }, response...)

	client.Handler.Out <-response
}

func requestClearData(client *shared.Client) {
	log.Printf("Func: requesting clear data.")

	lastMsgIdbytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(lastMsgIdbytes, uint32(client.LastMsgId))

	response := []byte { 0x87, 0x04, 0x00, }
	response = append(response, lastMsgIdbytes...)

	crc := calcCrcOfBytes(response)
	response = append(response, crc[1], crc[0])

	response = addEscBytes(response)
	response = append([]byte { 0xC0 }, response...)

	client.Handler.Out <-response
}

func checkCrc(pkg []byte) {
	pkgLength := len(pkg)
	if pkgLength < 2 {
		log.Printf("Warn: cannot calc CRC for package \n%s.", hex.Dump(pkg))
		return
	}

	log.Printf("Info: data for calculation of CRC: %s...%s.",
		hex.EncodeToString(pkg[:2]), hex.EncodeToString(pkg[pkgLength-4:pkgLength-2]))
	expected := pkg[pkgLength-2:pkgLength]

	result := calcCrcOfBytes(pkg[:pkgLength-2])

	if !bytes.Equal(expected, result) {
		log.Printf("Warn: expected CRC is %s but calculated is %s.", hex.EncodeToString(expected), hex.EncodeToString(result))
	} else {
		log.Printf("Info: expected CRC is correct.")
	}
}
