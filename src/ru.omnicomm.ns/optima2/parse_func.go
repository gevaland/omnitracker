package optima2
import (
	"log"
	"math"
	"encoding/hex"
	"encoding/binary"
)

type RecRegParser struct {
	Handler chan([]byte)
}

const (
	LENGTH = 1
	LENGTH2 = 2
	DATA = 3
	ID = 6
	FINISHED = 9
)

var mileages = map[uint]byte {}

// Parsing binary data received from registrar
func parseTransportData(data []byte) (uint, [][]byte) {
	dataLength := uint(len(data))
	log.Printf("Func: parsing data %s...%s of length %d.",
		hex.EncodeToString(data[0:2]), hex.EncodeToString(data[dataLength -2:dataLength]), dataLength)

	if len(data) < 13 {
		log.Fatalf("Warning: package length is %d, but minimal length is 13.", len(data))
	}

	var receivedMsgs [][]byte

	// 4 bytes
	msgId := uint(binary.LittleEndian.Uint32(data[0:4]))
	// +4 bytes
	// time := binary.LittleEndian.Uint32(data[4:8])

	// 1 byte
	log.Printf("Info: found reserved byte %s, skipped.",  hex.EncodeToString([]byte {data[8]}))

	var msg []byte = make([]byte, 0)
	var msgLength uint
	state := LENGTH
	for index, oneByteOfData := range data[9:dataLength] {

		uintOneByte := uint(oneByteOfData)

		switch state {
		// 1 byte
		case LENGTH:
			msgLength = uintOneByte & 0xFF
			state = LENGTH2
		// +1 byte
		case LENGTH2:
			msgLength |= ( ( uintOneByte & 0xFF ) << 8 )
			if msgLength != 0 {
				state = DATA
			}

			log.Printf("Info: msg length is %d.", msgLength)
		// 1 byte
		case DATA:
			msg = append(msg, oneByteOfData)

			if uint(index + 1) == msgLength + 2 { // 2 bytes for LENGTH, LENGTH2
				receivedMsgs = append(receivedMsgs, msg)
				log.Printf("Info: parsed msg %s...%s of length %d.",
					hex.EncodeToString(msg[0:2]), hex.EncodeToString(msg[len(msg)-2:len(msg)]), len(msg))
				state = LENGTH
			}
		case FINISHED:
			log.Fatalf("Fail: unexpected end of msg.")
		}
	}

	log.Printf("End: parsed data.")

	return msgId, receivedMsgs
}

// Ported from CS. Will be used to correct mileage if needed.
// 1st 4 bytes is msg ID,
// next 2 bytes is flags,
// next 3 bytes is run miles on,
// calculation starting from 6th byte of data.
func fixAbsoluteMileage(id uint, body []byte) []byte {
	currentValue := body[6] & 0xFF;
	currentValue |= (body[7] & 0xFF) << 8
	currentValue |= (body[8] & 0xFF) << 16

	prevValue, contains := mileages[id]
	mileages[id] = currentValue

	if (!contains) {
		prevValue = 0
	}

	var fixed byte
	if currentValue >= prevValue {
		fixed = currentValue - prevValue
	} else {
		fixed = (byte(math.Pow(2, 24))) - 1 + currentValue - prevValue
	}

	body[6] = fixed & 0xFF
	body[7] = (fixed >> 8) & 0xFF
	body[8] = (fixed >> 16) & 0xFF

	return body
}
