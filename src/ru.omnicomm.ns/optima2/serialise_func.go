package optima2

import (
	"log"
	"encoding/hex"
	"time"
	"github.com/golang/protobuf/proto"
	"menteslibres.net/gosexy/to"
	"ru.omnicomm.ns/shared"
	"ru.omnicomm.ns/descriptor"
	"strconv"
)

func deserialiseToRecReg(data []byte) *RecReg {
	log.Printf("Func: started to deserialize data %s...%s of length %d.",
		hex.EncodeToString(data[0:2]), hex.EncodeToString(data[len(data) - 2:len(data)]), len(data))

	msg := &RecReg{}
	if len(data) == 0 {
		log.Print("Warn: data is empty.")
	} else if err := proto.Unmarshal(data, msg); err != nil {
		log.Print("Error: cannot deserialise RecReg msg: ", err)
	}

	log.Print("End: to deserialise RecReg msg ", msg)
	return msg;
}

func serialiseToNsBinaryDataMessage(recReg *RecReg, terminalId *uint, lastMsgId *uint) []byte {
	receiveDate := time.Now().Unix()

	deviceMessageId := lastMsgId // *recReg.General.Time
	deviceMeasurementDate := (int(*recReg.General.Time) + 1230768000) * 1000

	deviceBinaryDataMessage := &DeviceBinaryDataMessage{}
	if recReg.Llsdt != nil {
		deviceBinaryDataMessage.TLLS1 = recReg.Llsdt.TLLS1
		deviceBinaryDataMessage.CLLS1 = recReg.Llsdt.CLLS1
		deviceBinaryDataMessage.FLLS1 = recReg.Llsdt.FLLS1
		deviceBinaryDataMessage.TLLS2 = recReg.Llsdt.TLLS2
		deviceBinaryDataMessage.CLLS2 = recReg.Llsdt.CLLS2
		deviceBinaryDataMessage.FLLS2 = recReg.Llsdt.FLLS2
		deviceBinaryDataMessage.TLLS3 = recReg.Llsdt.TLLS3
		deviceBinaryDataMessage.CLLS3 = recReg.Llsdt.CLLS3
		deviceBinaryDataMessage.FLLS3 = recReg.Llsdt.FLLS3
		deviceBinaryDataMessage.TLLS4 = recReg.Llsdt.TLLS4
		deviceBinaryDataMessage.CLLS4 = recReg.Llsdt.CLLS4
		deviceBinaryDataMessage.FLLS4 = recReg.Llsdt.FLLS4
		deviceBinaryDataMessage.TLLS5 = recReg.Llsdt.TLLS5
		deviceBinaryDataMessage.CLLS5 = recReg.Llsdt.CLLS5
		deviceBinaryDataMessage.FLLS5 = recReg.Llsdt.FLLS5
		deviceBinaryDataMessage.TLLS6 = recReg.Llsdt.TLLS6
		deviceBinaryDataMessage.CLLS6 = recReg.Llsdt.CLLS6
		deviceBinaryDataMessage.FLLS6 = recReg.Llsdt.FLLS6
		deviceBinaryDataMessage.TLLS7 = recReg.Llsdt.TLLS7
		deviceBinaryDataMessage.CLLS7 = recReg.Llsdt.CLLS7
		deviceBinaryDataMessage.FLLS7 = recReg.Llsdt.FLLS7
		deviceBinaryDataMessage.TLLS8 = recReg.Llsdt.TLLS8
		deviceBinaryDataMessage.CLLS8 = recReg.Llsdt.CLLS8
		deviceBinaryDataMessage.FLLS8 = recReg.Llsdt.FLLS8
	}

	deviceBinaryDataMessageInProto, err := proto.Marshal(deviceBinaryDataMessage)
	if err != nil {
		log.Printf("Error: unable to marshal DeviceBinaryDataMessage to proto.")
	}

	nsBinaryDataMessage := &descriptor.NsBinaryDataMessage{
		proto.Int64(int64(shared.GetAppConfig().NsGroupId)),
		proto.Int64(int64(receiveDate)),
		proto.String(strconv.Itoa(int(*terminalId))),
		proto.Int32(int32(*deviceMessageId)),
		proto.Int64(to.Int64(deviceMeasurementDate)),
		deviceBinaryDataMessageInProto,
		nil,
	}

	nsBinaryDataMessageInProto, err := proto.Marshal(nsBinaryDataMessage)
	if err != nil {
		log.Print("Error: cannot serialise RecReg msg: ", err)
	}

	return nsBinaryDataMessageInProto
}
