// Code generated by protoc-gen-go.
// source: Optima2DeviceDescriptor.proto
// DO NOT EDIT!

/*
Package optima2 is a generated protocol buffer package.

It is generated from these files:
	Optima2DeviceDescriptor.proto

It has these top-level messages:
	DeviceBinaryDataMessage
*/
package optima2

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type DeviceBinaryDataMessage struct {
	TLLS1            *int32  `protobuf:"zigzag32,1,opt,name=TLLS1" json:"TLLS1,omitempty"`
	CLLS1            *uint32 `protobuf:"varint,2,opt,name=CLLS1" json:"CLLS1,omitempty"`
	FLLS1            *int32  `protobuf:"zigzag32,3,opt,name=FLLS1" json:"FLLS1,omitempty"`
	TLLS2            *int32  `protobuf:"zigzag32,4,opt,name=TLLS2" json:"TLLS2,omitempty"`
	CLLS2            *uint32 `protobuf:"varint,5,opt,name=CLLS2" json:"CLLS2,omitempty"`
	FLLS2            *int32  `protobuf:"zigzag32,6,opt,name=FLLS2" json:"FLLS2,omitempty"`
	TLLS3            *int32  `protobuf:"zigzag32,7,opt,name=TLLS3" json:"TLLS3,omitempty"`
	CLLS3            *uint32 `protobuf:"varint,8,opt,name=CLLS3" json:"CLLS3,omitempty"`
	FLLS3            *int32  `protobuf:"zigzag32,9,opt,name=FLLS3" json:"FLLS3,omitempty"`
	TLLS4            *int32  `protobuf:"zigzag32,10,opt,name=TLLS4" json:"TLLS4,omitempty"`
	CLLS4            *uint32 `protobuf:"varint,11,opt,name=CLLS4" json:"CLLS4,omitempty"`
	FLLS4            *int32  `protobuf:"zigzag32,12,opt,name=FLLS4" json:"FLLS4,omitempty"`
	TLLS5            *int32  `protobuf:"zigzag32,13,opt,name=TLLS5" json:"TLLS5,omitempty"`
	CLLS5            *uint32 `protobuf:"varint,14,opt,name=CLLS5" json:"CLLS5,omitempty"`
	FLLS5            *int32  `protobuf:"zigzag32,15,opt,name=FLLS5" json:"FLLS5,omitempty"`
	TLLS6            *int32  `protobuf:"zigzag32,16,opt,name=TLLS6" json:"TLLS6,omitempty"`
	CLLS6            *uint32 `protobuf:"varint,17,opt,name=CLLS6" json:"CLLS6,omitempty"`
	FLLS6            *int32  `protobuf:"zigzag32,18,opt,name=FLLS6" json:"FLLS6,omitempty"`
	TLLS7            *int32  `protobuf:"zigzag32,19,opt,name=TLLS7" json:"TLLS7,omitempty"`
	CLLS7            *uint32 `protobuf:"varint,20,opt,name=CLLS7" json:"CLLS7,omitempty"`
	FLLS7            *int32  `protobuf:"zigzag32,21,opt,name=FLLS7" json:"FLLS7,omitempty"`
	TLLS8            *int32  `protobuf:"zigzag32,22,opt,name=TLLS8" json:"TLLS8,omitempty"`
	CLLS8            *uint32 `protobuf:"varint,23,opt,name=CLLS8" json:"CLLS8,omitempty"`
	FLLS8            *int32  `protobuf:"zigzag32,24,opt,name=FLLS8" json:"FLLS8,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *DeviceBinaryDataMessage) Reset()                    { *m = DeviceBinaryDataMessage{} }
func (m *DeviceBinaryDataMessage) String() string            { return proto.CompactTextString(m) }
func (*DeviceBinaryDataMessage) ProtoMessage()               {}
func (*DeviceBinaryDataMessage) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *DeviceBinaryDataMessage) GetTLLS1() int32 {
	if m != nil && m.TLLS1 != nil {
		return *m.TLLS1
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS1() uint32 {
	if m != nil && m.CLLS1 != nil {
		return *m.CLLS1
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS1() int32 {
	if m != nil && m.FLLS1 != nil {
		return *m.FLLS1
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS2() int32 {
	if m != nil && m.TLLS2 != nil {
		return *m.TLLS2
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS2() uint32 {
	if m != nil && m.CLLS2 != nil {
		return *m.CLLS2
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS2() int32 {
	if m != nil && m.FLLS2 != nil {
		return *m.FLLS2
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS3() int32 {
	if m != nil && m.TLLS3 != nil {
		return *m.TLLS3
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS3() uint32 {
	if m != nil && m.CLLS3 != nil {
		return *m.CLLS3
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS3() int32 {
	if m != nil && m.FLLS3 != nil {
		return *m.FLLS3
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS4() int32 {
	if m != nil && m.TLLS4 != nil {
		return *m.TLLS4
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS4() uint32 {
	if m != nil && m.CLLS4 != nil {
		return *m.CLLS4
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS4() int32 {
	if m != nil && m.FLLS4 != nil {
		return *m.FLLS4
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS5() int32 {
	if m != nil && m.TLLS5 != nil {
		return *m.TLLS5
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS5() uint32 {
	if m != nil && m.CLLS5 != nil {
		return *m.CLLS5
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS5() int32 {
	if m != nil && m.FLLS5 != nil {
		return *m.FLLS5
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS6() int32 {
	if m != nil && m.TLLS6 != nil {
		return *m.TLLS6
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS6() uint32 {
	if m != nil && m.CLLS6 != nil {
		return *m.CLLS6
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS6() int32 {
	if m != nil && m.FLLS6 != nil {
		return *m.FLLS6
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS7() int32 {
	if m != nil && m.TLLS7 != nil {
		return *m.TLLS7
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS7() uint32 {
	if m != nil && m.CLLS7 != nil {
		return *m.CLLS7
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS7() int32 {
	if m != nil && m.FLLS7 != nil {
		return *m.FLLS7
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetTLLS8() int32 {
	if m != nil && m.TLLS8 != nil {
		return *m.TLLS8
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetCLLS8() uint32 {
	if m != nil && m.CLLS8 != nil {
		return *m.CLLS8
	}
	return 0
}

func (m *DeviceBinaryDataMessage) GetFLLS8() int32 {
	if m != nil && m.FLLS8 != nil {
		return *m.FLLS8
	}
	return 0
}

func init() {
	proto.RegisterType((*DeviceBinaryDataMessage)(nil), "ru.omnicomm.ns.optima2.DeviceBinaryDataMessage")
}

func init() { proto.RegisterFile("Optima2DeviceDescriptor.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 229 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x4c, 0xd1, 0xbf, 0x4b, 0xc3, 0x40,
	0x18, 0xc6, 0x71, 0x6a, 0xad, 0xb5, 0xd1, 0xa8, 0x8d, 0xda, 0x3e, 0x8b, 0x20, 0x4e, 0x4e, 0x01,
	0xf3, 0x7b, 0xae, 0xc1, 0xa9, 0xe2, 0xa0, 0x93, 0xdb, 0x11, 0x0e, 0xb9, 0xe1, 0x72, 0xe1, 0xee,
	0x14, 0x1c, 0xfd, 0xcf, 0xf5, 0x1a, 0x0b, 0xcf, 0xf8, 0xe5, 0xf9, 0xbc, 0xd3, 0x1b, 0xdd, 0xbc,
	0x0c, 0x5e, 0x69, 0x91, 0xb5, 0xf2, 0x4b, 0x75, 0xb2, 0x95, 0xae, 0xb3, 0x6a, 0xf0, 0xc6, 0xa6,
	0x83, 0x35, 0xde, 0x24, 0x2b, 0xfb, 0x99, 0x1a, 0xdd, 0xab, 0xce, 0x68, 0x9d, 0xf6, 0x2e, 0x35,
	0xa3, 0xbe, 0xfb, 0x99, 0x46, 0xeb, 0xf1, 0x64, 0xa3, 0x7a, 0x61, 0xbf, 0x5b, 0xe1, 0xc5, 0xb3,
	0x74, 0x4e, 0x7c, 0xc8, 0x24, 0x8e, 0x66, 0x6f, 0xdb, 0xed, 0xeb, 0x03, 0x26, 0xb7, 0x93, 0xfb,
	0x65, 0xc8, 0xc7, 0x5d, 0x1e, 0xfc, 0x65, 0x1c, 0xf2, 0x69, 0x97, 0xd3, 0xfd, 0x1a, 0x70, 0x86,
	0x43, 0xc6, 0x19, 0x66, 0x8c, 0x33, 0x1c, 0x31, 0xce, 0x31, 0x67, 0x9c, 0xe3, 0x98, 0x71, 0x8e,
	0x05, 0xe3, 0x02, 0x11, 0xe3, 0x02, 0x27, 0x8c, 0x0b, 0x9c, 0x32, 0x2e, 0x11, 0x33, 0x2e, 0x71,
	0xc6, 0xb8, 0xc4, 0x39, 0xe3, 0x0a, 0x17, 0x8c, 0x2b, 0x2c, 0x19, 0x57, 0x48, 0x18, 0xd7, 0xb8,
	0x64, 0x5c, 0xe3, 0x8a, 0x71, 0x8d, 0x6b, 0xc6, 0x0d, 0x56, 0x8c, 0x1b, 0xac, 0x19, 0x37, 0x40,
	0x58, 0x37, 0x8b, 0xf7, 0xf9, 0xff, 0x3b, 0x7e, 0x03, 0x00, 0x00, 0xff, 0xff, 0x12, 0xb1, 0x1c,
	0xf5, 0xc6, 0x01, 0x00, 0x00,
}
