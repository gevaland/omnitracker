package optima2
import (
	"testing"
	"bytes"
)

func TestCalcByte80(t *testing.T) {
	data := []byte {
		0x80,0x08,0x00,0x09,0xe6,0xdf,0x0c,0x34,0x2d,0x00,0x00,
	}

	expected := []byte { 0x3f,0x32, }
	result := calcCrcOfBytes(data)

	if !bytes.Equal(expected, result)  {
		t.Fatalf("\nExpected %d \nResult %d", expected, result)
	}
}

func TestCalcByte81(t *testing.T) {
	data := []byte {
		0x81,0x08,0x00,0x0b,0x00,0x00,0x00,0x16,0x00,0x00,0x00,
	}

	expected := []byte { 0x25,0x36, } // reverse order!!! source is 0x36,0x25,
	result := calcCrcOfBytes(data)

	if !bytes.Equal(expected, result)  {
		t.Fatalf("\nExpected %d \nResult %d", expected, result)
	}
}

func TestCalcByte82(t *testing.T) {
	data := []byte {
		0x82,0x10,0x00,0x25,0xb3,0x17,0x5f,0xc3,0xa5,0x08,0x49,0xa0,0x3e,0xd3,0x06,0x01,0xfd,0x17,0xeb,
	}

	expected := []byte { 0x0a,0x01, } // reverse order!!! source is 0x01,0x0a,
	result := calcCrcOfBytes(data)

	if !bytes.Equal(expected, result)  {
		t.Fatalf("\nExpected %d \nResult %d", expected, result)
	}
}
