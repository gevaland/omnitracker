package dispatcher
import (
	"ru.omnicomm.ns/optima2"
	"ru.omnicomm.ns/shared"
)

// New protocols will be added here
func DispatchClientHandle(client *shared.Client) {
	optima2.Handle(client)
}
