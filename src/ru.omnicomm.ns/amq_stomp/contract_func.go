package amq_stomp
import (
	"log"
	"ru.omnicomm.ns/shared"
)

func SendContract(data *[]byte) error {
	return SendContractOnAddr(data, &shared.GetAppConfig().StompAddr, &shared.GetAppConfig().StompPort)
}

func SendContractOnAddr(data *[]byte, addr *string, port *uint) error {
	stompConn, err := connect(addr, port)
	if err != nil {
		return err
	}

	err = send(*data, &shared.GetAppConfig().ContractTopicName, stompConn)
	if err != nil {
		log.Fatalf("Cannot send the contract.")
	}
	stompConn.Disconnect()

	return nil
}

func ReceiveContractFromAddr(addr *string, port *uint) ([]byte, error) {
	return receive(&shared.GetAppConfig().ContractTopicName, addr, port)
}
