package amq_stomp
import (
	"log"
	"ru.omnicomm.ns/shared"
)

// Implements STOMP protocol. Details: http://activemq.apache.org/stomp.html
func SendMessage(data []byte) error {
	return SendMessageOnAddr(data, &shared.GetAppConfig().StompAddr, &shared.GetAppConfig().StompPort)
}

func SendMessageOnAddr(data []byte, addr *string, port *uint) error {
    stompConn, err := connect(addr, port)
    if err != nil {
        return err
    }

    err = send(data, &shared.GetAppConfig().DataQueueName, stompConn)
    if err != nil {
        log.Fatalf("Cannot send the data.")
    }
    stompConn.Disconnect()

    return nil
}

func ReceiveDataFromAddr(addr *string, port *uint) ([]byte, error) {
    return receive(&shared.GetAppConfig().DataQueueName, addr, port)
}
