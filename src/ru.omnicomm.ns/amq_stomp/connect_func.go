package amq_stomp

import (
    "log"
    "github.com/go-stomp/stomp"
    jstomp "github.com/jjeffery/stomp"
    "strconv"
    "encoding/hex"
)

// Implements STOMP protocol. Details: http://activemq.apache.org/stomp.html
func connect(addr *string, port *uint) (*stomp.Conn, error) {
    conn, err := stomp.Dial("tcp", *addr + ":" + strconv.Itoa(int(*port)))
    if err != nil {
        log.Fatalf("Fail: cannot connect to STOMP. %s", err.Error())
    }

    return conn, err
}

// Implements STOMP protocol. Details: http://activemq.apache.org/stomp.html
func send(bytes []byte, destination *string, conn *stomp.Conn) error {
    log.Printf("Func: send data to STOMP.");

    tx := conn.Begin()

    log.Printf("Info: sending binary data \n%s.", hex.Dump(bytes));
    err := conn.Send(
        *destination, // destination
        "application/octet-stream", // content-type
        bytes, // body
        stomp.SendOpt.Receipt,
        //stomp.SendOpt.Header("expires", "2020-12-12 23:59:59"),
    )
    if err != nil {
        return err
    }

    tx.Commit()
    if err != nil {
        return err
    }

    return nil
}

func receiveWithJStomp(destination *string, addr *string, port *uint) ([]byte, error) {
    log.Printf("Func: receive from STOMP (%s:%d).", *addr, *port)

    conn, err := jstomp.Dial("tcp", *addr + ":" + strconv.Itoa(int(*port)), jstomp.Options{})
    if err != nil {
        return make([]byte, 0), err
    }

    sub, err := conn.Subscribe("/topic/CONTRACT", jstomp.AckAuto)
    if err != nil {
        return make([]byte, 0), err
    }
    msg := <- sub.C

    return msg.Body, err
}

func receive(destination *string, addr *string, port *uint) ([]byte, error) {
    conn, err := connect(addr, port)

    tx := conn.Begin()

    sub, err := conn.Subscribe(*destination, stomp.AckAuto,
        stomp.SubscribeOpt.Header("activemq.prefetchSize", "1"),
        stomp.SubscribeOpt.Header("activemq.retroactive", "true"))
    if err != nil {
        return make([]byte, 0), err
    }

    msg, err := sub.Read()
    if err != nil {
        return make([]byte, 0), err
    }

    tx.Ack(msg)
    // acknowledge the message
    err = conn.Ack(msg)
    if err != nil {
        return msg.Body, err
    }

    err = sub.Unsubscribe()
    if err != nil {
        return make([]byte, 0), err
    }

    err = tx.Commit()
    if err != nil {
        return make([]byte, 0), err
    }

    err = conn.Disconnect()
    if err != nil {
        return make([]byte, 0), err
    }

    return msg.Body, err
}
