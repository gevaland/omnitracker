package descriptor

import (
    "log"
    "ru.omnicomm.ns/shared"
    "github.com/golang/protobuf/proto"
    "io/ioutil"
)

func NewContract() *[]byte {
    log.Printf("Info: NS group ID is %d.", shared.GetAppConfig().NsGroupId)
    nsGroupId := int64(shared.GetAppConfig().NsGroupId)
    nsBinaryDataDescriptor, err := ioutil.ReadFile("Optima2DeviceDescriptor.desc")
    if err != nil {
        log.Fatalf("Fail: cannot read the Optima2DeviceDescriptor.desc: %s", err.Error())
    }

    contract := &NsContractMessage{
        &nsGroupId,
        nsBinaryDataDescriptor,
        nil,
    }
    contractBytes, err := proto.Marshal(contract)
    if err != nil {
        log.Fatalf("Fail: cannot marshal the contract: %s", err.Error())
    }

    return &contractBytes
}
