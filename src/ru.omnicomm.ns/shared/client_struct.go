package shared

type Client struct {
	Id        uint
	Handler   Handler
	Token     []byte
	LastMsgId uint
}

type Handler struct {
	InReady  chan bool
	In       chan []byte
	Out      chan []byte
}

func NewClient() *Client {
	handler := Handler{ make(chan bool), make(chan []byte), make(chan []byte) }
	return &Client{ 0, handler, make([]byte, 16), 0 }
}
