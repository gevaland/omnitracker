package shared
import (
	"log"
	"io/ioutil"
	"encoding/json"
)

type AppConfig struct {
	ServerAddr string
	ServerPort uint
	StompAddr string
	StompPort uint
	ContractTopicName string
	DataQueueName string
	RedisAddr string
	RedisPort uint
	NsGroupId uint
}

const default_config_name = "config.json"
var appConfig *AppConfig

func GetAppConfig() *AppConfig {
	if appConfig != nil {
		return appConfig
	}

	jsonConfigBytes, err := ioutil.ReadFile(default_config_name)
	if err != nil {
		log.Fatalf("Fail to read config. %v", err)
	}

	appConfig = &AppConfig{}
	log.Printf("Read app config: %v.", string(jsonConfigBytes))
	err = json.Unmarshal(jsonConfigBytes, appConfig)
	if err != nil {
		log.Fatalf("Fail to deserialise config. %v", err)
	}

	return appConfig
}
