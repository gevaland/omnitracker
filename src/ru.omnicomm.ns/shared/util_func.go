package shared
import (
	"encoding/binary"
	"log"
)

func Make4bytes(i uint) []byte {
	result := make([]byte, 4)
	binary.LittleEndian.PutUint32(result[0:], uint32(i))

	return result
}

func Make4x4bytes(value uint) []byte {
	result := make([]byte, 16)
	i := uint32(value)
	binary.LittleEndian.PutUint32(result[0:], i)
	binary.LittleEndian.PutUint32(result[4:], i)
	binary.LittleEndian.PutUint32(result[8:], i)
	binary.LittleEndian.PutUint32(result[12:], i)

	return result
}

func ExtractTerminalId(data[] byte) uint {
	length := len(data)
	if length != 8 {
		log.Fatalf("Terminal ID is incorrect length: %d.", length)
	}

	return uint(binary.LittleEndian.Uint32(data))
}
