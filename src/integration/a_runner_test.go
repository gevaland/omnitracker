package integration

import (
    "testing"
    "ru.omnicomm.ns/shared"
    "ru.omnicomm.ns/amq_stomp"
    "ru.omnicomm.ns/descriptor"
    "bytes"
)

func TestStartServer0(t *testing.T) {
    t.Log(shared.GetAppConfig())
    startServer()
}

// FIXME: AMQ: consume every one object without timeout
func testStompContractReceive(t *testing.T) {

    expected := *descriptor.NewContract()

    result, err := amq_stomp.ReceiveContractFromAddr(&shared.GetAppConfig().StompAddr, &shared.GetAppConfig().StompPort)
    if err != nil {
        t.Fatalf("Fail: cannot receive from STOMP: '%s'.", err.Error())
    }

    if bytes.Equal(expected, result) {
        t.Log("OK")
    } else {
        t.Fatalf("Expected\n%s\nResult\n%s", expected, result)
    }
}
