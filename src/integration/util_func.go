package integration

import (
	"net"
	"strconv"
	"log"
	"bufio"
	"encoding/hex"
	"ru.omnicomm.ns/shared"
	"ru.omnicomm.ns/server"
	"ru.omnicomm.ns/storage"
    "ru.omnicomm.ns/amq_stomp"
)

func startServer() {
	go server.Run()
	waitForServerListening()
	storage.WriteLastMessageIndex(0, 0)
}

func waitForServerListening() {
	for {
		c := shared.GetAppConfig()
		if listener, err := net.Dial("tcp", c.ServerAddr + ":" + strconv.Itoa(int(c.ServerPort))); err != nil {
			log.Printf("Info: cannot start server.")
		} else {
			listener.Close()
			log.Printf("Info: server is started.")
			break
		}
	}
}

func sendDataToServerAndReceiveByTerminal(data []byte, conn net.Conn) []byte {
	var result []byte

	sendDataToServer(data, conn)

	reader := bufio.NewReader(conn)
	result = make([]byte, 1024)
	if bytesRead, err := reader.Read(result); err != nil {
		log.Fatal("Fail: cannot read data input.")
	} else {
		result = result[:bytesRead]
		log.Print("Info: received answer from server.")
	}

	if err := conn.Close(); err != nil {
		log.Fatal("Fail: cannot close connection.")
	}

	return result
}

func sendDataToServer(data []byte, conn net.Conn) {
	log.Print("Sending data to test \n", hex.Dump(data))

	writer := bufio.NewWriter(conn)
	for i := 0; i < len(data); i += 5 {
		if i + 5 < len(data) {
			writer.Write(data[i:i+5])
		} else {
			writer.Write(data[i:len(data)])
		}
	}
	defer writer.Flush()
}

func receiveDataFromStomp(addr string, port uint) {
    amq_stomp.ReceiveDataFromAddr(&addr, &port)
}

func sendDataToServerAndReceiveFromStomp(data []byte, conn net.Conn) []byte {
	var result []byte

	sendDataToServer(data, conn)

	reader := bufio.NewReader(conn)
	result = make([]byte, 1024)
	if bytesRead, err := reader.Read(result); err != nil {
		log.Fatal("Fail: cannot read data input.")
	} else {
		result = result[:bytesRead]
		log.Print("Info: received answer from server.")
	}

	if err := conn.Close(); err != nil {
		log.Fatal("Fail: cannot close connection.")
	}

	return result
}
