#!/usr/bin/env sh
set +x

if [ ! -d "$GOROOT" ]
then
    export GOROOT='/opt/go'
    export PATH="$PATH:$GOPATH/bin:$GOROOT/bin"
fi

if [ ! -d "$GOPATH" ]
then
    export GOPATH=`pwd`
fi
if [ ! -d "$GOPATH/src/" ]
then
    export GOPATH=`pwd`'/../'
fi

go get github.com/golang/protobuf/proto
go get github.com/golang/protobuf/protoc-gen-go
go get github.com/go-stomp/stomp
go get github.com/jjeffery/stomp
go get github.com/gosexy/redis

protoc --go_out=$GOPATH/src/ru.omnicomm.ns/descriptor/ --proto_path=$GOPATH/src/proto/ru/omnicomm/ns/descriptor/ $GOPATH/src/proto/ru/omnicomm/ns/descriptor/NsDescriptor.proto
protoc --go_out=$GOPATH/src/ru.omnicomm.ns/optima2/ --proto_path=$GOPATH/src/proto/ru/omnicomm/ns/optima2/ $GOPATH/src/proto/ru/omnicomm/ns/optima2/Optima2DeviceDescriptor.proto
protoc --descriptor_set_out=$GOPATH/src/integration/Optima2DeviceDescriptor.desc --proto_path=$GOPATH/src/proto/ru/omnicomm/ns/optima2/ $GOPATH/src/proto/ru/omnicomm/ns/optima2/Optima2DeviceDescriptor.proto

mkdir -p "$GOPATH/bin/"
go build -o "$GOPATH/bin/omnicomm-optima2" ru.omnicomm.ns
cp $GOPATH/src/integration/config.json $GOPATH/bin/
cp $GOPATH/src/integration/Optima2DeviceDescriptor.desc $GOPATH/bin/

go test -a ru.omnicomm.ns/...
go test -a integration
